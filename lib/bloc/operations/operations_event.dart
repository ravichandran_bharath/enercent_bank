part of 'operations_bloc.dart';

abstract class OperationsEvent {}

class InitialEvent extends OperationsEvent {
  final int key;

  InitialEvent(this.key);
}

class WithdrawEvent extends OperationsEvent {
  final int key;
  final Map<String, dynamic> item;

  WithdrawEvent(this.key, this.item);
}

class DepositEvent extends OperationsEvent {
  final int key;
  final Map<String, dynamic> item;

  DepositEvent(this.key, this.item);
}
