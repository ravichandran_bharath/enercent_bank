part of 'operations_bloc.dart';

abstract class OperationsState {
  const OperationsState();
}

class OperationsInitialState extends OperationsState {}

class OperationsLoadingState extends OperationsState {}

class OperationsSuccessState extends OperationsState {
  final Map<String, dynamic> item;

  OperationsSuccessState(this.item);
}

class OperationsErrorState extends OperationsState {
  final error;

  OperationsErrorState(this.error);
}
