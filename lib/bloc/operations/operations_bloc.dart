import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:hive/hive.dart';

part 'operations_event.dart';
part 'operations_state.dart';

class OperationsBloc extends Bloc<OperationsEvent, OperationsState> {
  final _enercentBank = Hive.box('enercent_bank');

  OperationsBloc() : super(OperationsInitialState()) {
    on<InitialEvent>((event, emit) => _init(emit, event.key));
    on<WithdrawEvent>((event, emit) => _withdraw(emit, event.key, event.item));
    on<DepositEvent>((event, emit) => _deposit(emit, event.key, event.item));
  }

  void _init(Emitter emit, int key) async {
    emit(OperationsLoadingState());
    try {
      final item = await _enercentBank.get(key);
      emit(OperationsSuccessState(item));
    } catch (e) {
      emit(OperationsErrorState(e));
    }
  }

  void _withdraw(Emitter emit, int key, Map<String, dynamic> item) async {
    emit(OperationsLoadingState());
    try {
      await _enercentBank.put(key, item);
      final res = await _enercentBank.get(key);
      emit(OperationsSuccessState(item));
    } catch (e) {
      emit(OperationsErrorState(e));
    }
  }

  void _deposit(Emitter emit, int key, Map<String, dynamic> item) async {
    emit(OperationsLoadingState());
    try {
      await _enercentBank.put(key, item);
      final val = await _enercentBank.get(key);
      val['key'] = key;
      emit(OperationsSuccessState(val));
    } catch (e) {
      emit(OperationsErrorState(e));
    }
  }
}
