part of 'auth_bloc.dart';

abstract class AuthState {
  const AuthState();
}

class AuthInitialState extends AuthState {}

class AuthLoginState extends AuthState {}

class AuthenticatedState extends AuthState {
  final Map<String, dynamic> user;

  AuthenticatedState(this.user);
}

class AuthCreateUserState extends AuthState {}

class AuthSignUpState extends AuthState {}

class AuthErrorState extends AuthState {
  final error;

  AuthErrorState(this.error);
}
