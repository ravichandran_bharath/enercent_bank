part of 'auth_bloc.dart';

abstract class AuthEvent {}

class LoginEvent extends AuthEvent {
  final Map<String, dynamic> login;

  LoginEvent(this.login);
}

class LogOutEvent extends AuthEvent {}

class CreateUserEvent extends AuthEvent {}

class SignUpEvent extends AuthEvent {
  final Map<String, dynamic> signup;

  SignUpEvent(this.signup);
}
