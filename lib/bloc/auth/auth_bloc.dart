import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:hive/hive.dart';

part 'auth_event.dart';
part 'auth_state.dart';

class AuthBloc extends Bloc<AuthEvent, AuthState> {
  final _enercentBank = Hive.box('enercent_bank');

  AuthBloc() : super(AuthInitialState()) {
    on<LoginEvent>((event, emit) => _login(emit, event.login));
    on<LogOutEvent>((event, emit) => _logout(emit));
    on<CreateUserEvent>((event, emit) => _createUser(emit));
    on<SignUpEvent>((event, emit) => _signUp(emit, event.signup));
  }

  void _login(Emitter emit, Map<String, dynamic> login) async {
    emit(AuthLoginState());
    try {
      final data = _enercentBank.keys.map((key) {
        final value = _enercentBank.get(key);
        return {
          "key": key,
          "name": value["name"],
          "password": value['password'],
          "balance": value['balance']
        };
      }).toList();

      data.forEach((element) {
        final val1 = element.containsValue(login['name']);
        final val2 = element.containsValue(login['password']);
        if (val1 && val2) {
          final value = element;
          emit(AuthenticatedState(value));
        }
      });
    } catch (e) {
      emit(AuthErrorState(e));
    }
  }

  void _logout(Emitter emit) async {
    emit(AuthInitialState());
  }

  void _createUser(Emitter emit) async {
    emit(AuthCreateUserState());
  }

  void _signUp(Emitter emit, Map<String, dynamic> signup) async {
    emit(AuthSignUpState());
    try {
      final res = await _enercentBank.add(signup);
      if (res != null) {
        final item = _enercentBank.get(res);
        item['key'] = res;
        emit(AuthenticatedState(item));
      }
    } catch (e) {
      emit(AuthErrorState(e));
    }
  }
}
