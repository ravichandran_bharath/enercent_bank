import 'dart:io';

import 'package:enercent_bank/ui/home/home_screen.dart';
import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:path_provider/path_provider.dart' as provide_path;

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  Directory directory = await provide_path.getApplicationDocumentsDirectory();
  Hive.init(directory.path);
  await Hive.openBox('enercent_bank');
  runApp(HomeScreen());
}
