import 'package:enercent_bank/bloc/auth/auth_bloc.dart';
import 'package:enercent_bank/bloc/operations/operations_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class LandingScreen extends StatelessWidget {
  final Map<String, dynamic> user;
  final TextEditingController depositController = TextEditingController();
  final TextEditingController withdrawController = TextEditingController();

  LandingScreen({Key? key, required this.user}) : super(key: key);

  late int balance = 0;

  @override
  Widget build(BuildContext context) {
    balance = user['balance'];
    return BlocProvider(
      create: (BuildContext context) => OperationsBloc(),
      child: Scaffold(
        appBar: AppBar(
          title: const Text('Enercent Bank'),
          automaticallyImplyLeading: false,
        ),
        body: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Card(
                child: Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Column(
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          const CircleAvatar(
                            maxRadius: 30,
                            backgroundImage: NetworkImage(
                                'https://cdn.pixabay.com/photo/2015/04/23/22/00/tree-736885__480.jpg'),
                          ),
                          const SizedBox(width: 20),
                          Text(user['name'].toString()),
                        ],
                      ),
                      const SizedBox(height: 20),
                      BlocBuilder<OperationsBloc, OperationsState>(
                        builder: (_, state) {
                          if (state is OperationsInitialState) {
                            return Text(
                                'Balance : ' + user['balance'].toString());
                          } else if (state is OperationsSuccessState) {
                            balance = state.item['balance'];
                            return Text('Balance : ' +
                                state.item['balance'].toString());
                          }
                          return Text(
                              'Balance : ' + user['balance'].toString());
                        },
                      ),
                      const SizedBox(height: 20),
                      Builder(builder: (context) {
                        return Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            SizedBox(
                              width: 100,
                              child: OutlinedButton(
                                onPressed: () {
                                  withdrawDialog(
                                    label: 'Withdraw Amount',
                                    controller: withdrawController,
                                    name: user['name'].toString(),
                                    context: context,
                                  );
                                },
                                child: Text(
                                  'Withdraw',
                                  style: Theme.of(context)
                                      .textTheme
                                      .bodyText1!
                                      .copyWith(
                                        color: Colors.green,
                                      ),
                                ),
                                style: OutlinedButton.styleFrom(),
                              ),
                            ),
                            const SizedBox(width: 20),
                            SizedBox(
                              width: 100,
                              child: ElevatedButton(
                                onPressed: () {
                                  depositDialog(
                                    label: 'Deposit Amount',
                                    controller: depositController,
                                    name: user['name'].toString(),
                                    context: context,
                                  );
                                },
                                child: Text(
                                  'Deposit',
                                  style: Theme.of(context)
                                      .textTheme
                                      .bodyText1!
                                      .copyWith(
                                        color: Colors.white,
                                      ),
                                ),
                                style: ElevatedButton.styleFrom(
                                  primary: Colors.green,
                                ),
                              ),
                            ),
                          ],
                        );
                      }),
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
        floatingActionButton: SizedBox(
          width: 100,
          child: OutlinedButton(
            onPressed: () => context.read<AuthBloc>().add(LogOutEvent()),
            child: Text(
              'Logout',
              style: Theme.of(context).textTheme.bodyText1!.copyWith(
                    color: Colors.green,
                  ),
            ),
            style: OutlinedButton.styleFrom(),
          ),
        ),
      ),
    );
  }

  void withdrawDialog({
    required BuildContext context,
    required String name,
    required TextEditingController controller,
    required String label,
  }) {
    showDialog(
      context: context,
      builder: (_) {
        return AlertDialog(
          title: Text(name),
          content: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              TextFormField(
                controller: controller,
                decoration: InputDecoration(
                  labelText: label,
                  hintText: 'Enter Value Here',
                  fillColor: Colors.white,
                  focusedBorder: const OutlineInputBorder(
                    borderSide: BorderSide(
                      color: Colors.black12,
                    ),
                  ),
                  enabledBorder: const OutlineInputBorder(
                    borderSide: BorderSide(
                      color: Colors.black12,
                    ),
                  ),
                ),
              ),
              const SizedBox(height: 10),
              SizedBox(
                width: 100,
                child: OutlinedButton(
                  onPressed: () {
                    final res =
                        (balance - int.parse(controller.text.toString()));
                    if (controller.text != null) {
                      controller.clear();
                      Navigator.of(context).pop();
                      context.read<OperationsBloc>().add(
                            WithdrawEvent(
                              user['key'],
                              {
                                'key': user['key'],
                                'name': user['name'],
                                'password': user['password'],
                                'confirmPassword': user['confirmPassword'],
                                'balance': res
                              },
                            ),
                          );
                    }
                  },
                  child: Text(
                    'Submit',
                    style: Theme.of(context).textTheme.bodyText1!.copyWith(
                          color: Colors.green,
                        ),
                  ),
                  style: OutlinedButton.styleFrom(),
                ),
              ),
            ],
          ),
        );
      },
    );
  }

  void depositDialog({
    required BuildContext context,
    required String name,
    required TextEditingController controller,
    required String label,
  }) {
    showDialog(
      context: context,
      builder: (_) {
        return AlertDialog(
          title: Text(name),
          content: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              TextFormField(
                controller: controller,
                decoration: InputDecoration(
                  labelText: label,
                  hintText: 'Enter Value Here',
                  fillColor: Colors.white,
                  focusedBorder: const OutlineInputBorder(
                    borderSide: BorderSide(
                      color: Colors.black12,
                    ),
                  ),
                  enabledBorder: const OutlineInputBorder(
                    borderSide: BorderSide(
                      color: Colors.black12,
                    ),
                  ),
                ),
              ),
              const SizedBox(height: 10),
              SizedBox(
                width: 100,
                child: OutlinedButton(
                  onPressed: () {
                    final res = (balance + int.parse(controller.text));
                    controller.clear();
                    Navigator.of(context).pop();
                    context.read<OperationsBloc>().add(
                          DepositEvent(user['key'], {
                            'key': user['key'],
                            'name': user['name'],
                            'password': user['password'],
                            'confirmPassword': user['confirmPassword'],
                            'balance': res
                          }),
                        );
                  },
                  child: Text(
                    'Submit',
                    style: Theme.of(context).textTheme.bodyText1!.copyWith(
                          color: Colors.green,
                        ),
                  ),
                  style: OutlinedButton.styleFrom(),
                ),
              ),
            ],
          ),
        );
      },
    );
  }
}
