import 'package:enercent_bank/bloc/auth/auth_bloc.dart';
import 'package:enercent_bank/ui/auth/login_screen.dart';
import 'package:enercent_bank/ui/auth/signup_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'landing_screen.dart';

class HomeScreen extends StatelessWidget {
  HomeScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Enercent Assessment',
      theme: ThemeData(
        primarySwatch: Colors.green,
      ),
      // home: const LoginScreen(),
      home: BlocProvider<AuthBloc>(
        create: (context) => AuthBloc(),
        child: BlocBuilder<AuthBloc, AuthState>(
          builder: (_, state) {
            if (state is AuthInitialState) {
              return LoginScreen();
            } else if (state is AuthLoginState) {
              return LoginScreen();
            } else if (state is AuthCreateUserState) {
              return SignUpScreen();
            } else if (state is AuthSignUpState) {
              return SignUpScreen();
            } else if (state is AuthenticatedState) {
              return LandingScreen(
                user: state.user,
              );
            }
            return LoginScreen();
          },
        ),
      ),
    );
  }
}
